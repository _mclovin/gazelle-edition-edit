// ==UserScript==
// @name           Gazelle: Edition Edit
// @description    Apply changes to all torrents in the current edition
// @version        2.00
// @require        https://code.jquery.com/jquery-3.6.3.min.js
// @match          https://redacted.sh/torrents.php?action=edit&id=*
// @match          https://orpheus.network/torrents.php?action=edit&id=*
// @match          https://lztr.me/torrents.php?action=edit&id=*
// @run-at         document-end
// @namespace      _
// ==/UserScript==

(() => {
  "use strict";

  const torrentID = new URL(window.location).searchParams.get("id");
  const authKey = document
    .getElementById("nav_logout")
    .firstElementChild.href.match(/auth=([a-z0-9\-_]+)/i)[1];

  const webQuery = async (endpoint) => {
    try {
      const res = await fetch(endpoint);
      return res.text();
    } catch (error) {
      console.log(error);
    }
  };

  const queryAPI = async (id) => {
    try {
      const res = await fetch(`${location.origin}/ajax.php?action=torrent&id=${id}`);
      return res.json();
    } catch (error) {
      console.log(error);
    }
  };

  const decodeJSON = (payload) => {
    var txt = document.createElement("textarea");
    txt.innerHTML = payload;
    return txt.value;
  };

  const submitOtherForm = async (formData, torrentid) => {
    formData.append("auth", authKey);
    formData.append("action", "takeedit");
    const file = new Blob([
      JSON.stringify({})
    ], { type: 'application/json' });
    formData.append("logfiles[]", file);
    formData.append("torrentid", torrentid);
    formData.append("remaster", "on");
    formData.append("type", 1);
    formData.append("submit", true);
    const response = await fetch(
      `${location.origin}/torrents.php?action=edit&id=${torrentid}`,
      {
        method: "POST",
        body: formData,
      }
    );
    return response;
  };

  if (document.getElementById("remaster") && !document.getElementById("remaster").checked) {
    // OPS original release
    queryAPI(torrentID).then(({ response, status }) => {
      if (status == "success") {
        let { group, torrent } = response;
        if (!torrent.remastered) {
          document.getElementById("remaster_year").value = group.year;
          document.getElementById("remaster_record_label").value = decodeJSON(group.recordLabel);
          document.getElementById("remaster_catalogue_number").value = decodeJSON(group.catalogueNumber);
        }
        document.getElementById("remaster").click();
        // force this even if click() is mishandled
        $("#remaster_true").show();
      }
    });
  }

  const sameEditionIDs = [];
  webQuery(`${location.origin}/torrents.php?torrentid=${torrentID}`).then((response) => {
    const parser = new DOMParser();
    const doc = parser.parseFromString(response, "text/html");
    const editionClass = [...doc.getElementById(`torrent${torrentID}`).classList].find(value => /^edition_[0-9]+$/.test(value));
    [...doc.getElementsByClassName(`torrent_row ${editionClass}`)].forEach((torrent) => {
      let id = torrent.id.replace("torrent", "");
      if (id != torrentID) {
        sameEditionIDs.push(id);
      }
    });
    // add the button, border, and handler after collecting other same-edition torrent IDs
    [
      "remaster_year",
      "remaster_title",
      "remaster_record_label",
      "remaster_catalogue_number",
      "media",
    ].forEach((field) => {
      document.getElementById(field).style.border = "3px solid gray";
    });
    $("#post")
      .after($(`<input style="border: 3px solid gray;" id="post2" type="submit" value="Edit edition">`))
      .after($(`<span> | </span>`));
    document.getElementById("post2").addEventListener("click", handleSubmit);
  });

  const handleSubmit = async (event) => {
    event.preventDefault();
    event.target.removeEventListener("click", handleSubmit);
    const remaster_year = document.getElementById("remaster_year").value;
    const remaster_title = document.getElementById("remaster_title").value;
    const remaster_record_label = document.getElementById("remaster_record_label").value;
    const remaster_catalogue_number = document.getElementById("remaster_catalogue_number").value;
    const media = document.getElementById("media").value;
    const unknown = document.getElementById("unknown")?.checked
    for (let torrentid of sameEditionIDs) {
      let formData = new FormData();
      let parser = new DOMParser();
      let response = await webQuery(`${location.origin}/torrents.php?action=edit&id=${torrentid}`);
      let doc = parser.parseFromString(response, "text/html");
      doc.querySelectorAll('#dynamic_form input, #dynamic_form select, #freetorrent select, #dynamic_form textarea').forEach(elem => {
        if (elem.name != "" && elem.value) {
          if (elem.type == "checkbox" && elem.checked) {
            formData.append(elem.name, elem.value);
          } else if (elem.type != "checkbox") {
            formData.append(elem.name, elem.value);
          }
        }
        // overwrite where necessary
        formData.set("remaster_year", remaster_year);
        formData.set("remaster_title", remaster_title);
        formData.set("remaster_record_label", remaster_record_label);
        formData.set("remaster_catalogue_number", remaster_catalogue_number);
        formData.set("media", media);
        if (unknown) {
          formData.set("unknown", "on");
        }
      });
      let result = await submitOtherForm(formData, torrentid);
      if (!result.ok) {
        throw Error(`Failed to submit form to change edition for torrent id: ${torrentid} | ${result.statusText}.`);
      }
    }
    // now, submit the form we are actually on
    document
      .createElement("form")
      .submit.call(document.getElementById("upload_table"));
  };
})();